<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // This function will run depending if the route matches this function.
    public function create()
    {
        // It will return Tthe create.blade.php file
        return view('posts.create');
    }

    public function store(Request $request)
    {
        
        if(Auth::user()){
            // 1. Check if a user is Logged in
            $post = new Post;

            // 2. Assigns a value for each field in a post
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = Auth::user()->id;

            // 3. Save the newly assigned data to the database
            $post->save();

            // 4. Redirect to the /posts endpoint with a GET method
            return redirect('/posts');

        } else {
            return redirect('login');
        }
    }

    public function index()
    {
        // Retrieves All of the posts from the 'posts' table
        $posts = Post::all();

        // Returns the posts.index view along with ALL of the posts from the 'posts' table
        return view('posts.index')->with('posts', $posts);
    }

    public function show($id){

        // The selected post's ID will be passed here and used in the find() function of the Post model to find the specific post with that ID.
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    public function edit($id){
        $post = Post::find($id);

        return view('posts.edit')->with('post', $post);
    }

    public function update(Request $request, $id){

        // Get the existing post thru the ID
        $post = Post::find($id);

        // if statement checks if the logged in user is the author of the post
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');

            $post->save();
        }

        return redirect('/posts');
    }

    public function destroy($id){
        $post = Post::Find($id);

        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }

        return redirect('/posts');
    }

    public function like($id){

        // get the post to be liked and the user that user that liked that post
        $post = Post::find($id);
        $user_id = Auth::user()->id;


        // check if the author of the post is the same as the user trying to like the post
        if($post->user_id != $user_id){

            // Checks if the user has already liked the post, if so delete the like from the post
            if($post->likes->contains("user_id", $user_id)){
                PostLike::where('post_id', $post->id)->where('user_id', $user_id)
                ->delete();
            } else {
                $postLike = new PostLike;

                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            return redirect("/posts/$id");

        }  
    }

    public function comment(Request $request, $id)
    {
        
        // get the post to be commented and the user that user that commented that post
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if(Auth::user()){
            // 1. Check if a user is Logged in
            $comment = new PostComment;

            // 2. Assigns a value for each field in a post
            $comment->content = $request->input('content');
            $comment->post_id = $post->id;
            $comment->user_id = $user_id;

            // 3. Save the newly assigned data to the database
            $comment->save();

            // 4. Redirect to the /posts endpoint with a GET method
            return redirect("/posts/$id");

        } else {
            return redirect('login');
        }
    }
}
