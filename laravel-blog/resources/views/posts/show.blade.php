@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created At: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>

            @if(Auth::user())
                @if(Auth::id() != $post->user_id)
                    <form action="/posts/{{$post->id}}/like" class="d-inline" method="POST">
                        @method('PUT')
                        @csrf
                        @if($post->likes->contains('user_id', Auth::id()))
                            <button type="submit" class="btn btn-danger">Unlike</button>
                        @else
                            <button type="submit" class="btn btn-success">Like</button>
                        @endif
                    </form>
                @endif

                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Comment
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title" id="exampleModalLabel">Leave a Comment</h3>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                        <div class="modal-body">
                            <form method="POST" action="/posts/{{$post->id}}/comment">
                                @csrf       
                                    <div class="form-group">
                                        <textarea class="form-control" id="content" name="content" row="3"></textarea>
                                    </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Comment</button>
                        </div>
                        </form>
                    </div>
                </div>
            @endif
		    </div>
	    </div>
    </div>



    <div class="card mt-5">
		<div class="card-body">
            <h1>Comment Section</h1>
        </div>
    </div>  
@endsection